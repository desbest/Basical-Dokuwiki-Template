# Basical dokuwiki template

* Designed by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:basical)

![basical theme screenshot](https://i.imgur.com/NuLVxql.png)